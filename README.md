# Data Mining Course

`python` and `pip` version 3 is required to run this.

### Clone repository

```bash
https://gitlab.com/nikhilnathr-machine-learning/data-mining-course.git
cd data-mining-course
```

It is recomended to install the requirements in a virtual environment.


### Setup virtual environment (optional)
```bash
virtualenv -p python3 venv
source venv/bin/activate
```

### Install dependencies anf run jupyter notebook (or lab)

```bash
pip3 install -r requirements.txt
```
